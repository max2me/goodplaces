Installation instructions
-------------------------

Required applications
---------------------
* Visual Studio (2012 is preferred)
* ASP.NET MVC4
* Internet Information Services (IIS)
* SQL Server 2012 (express or better)


Installation Notes
------------------
* When installing SQL Server, be sure to have the server run off the default application

SQL Server Configuration
------------------------
* Go to sql server configuration manager
** Make sure SQL Server Network Configuration -> Protocols for SQLEXPRESS -> TCP/IP is enabled
** Right click on TCP/IP properties
** Ensure that IPALL (at the very bottom) has:
*** TCP Dyanamic Ports: nothing
*** TCP Port: 1433

Add goodplaces to hosts
-----------------------
* Go to WINDOWS/System32/Drivers/etc/hosts
** Add the line : 127.0.0.1 goodplaces.local

IIS Setup
------------------------
* Add goodplaces as a web site
** Right click on sites -> add website -> name it "goodplaces"
* Change application pools to network service and .net framework 4.0
** Application Pools -> right click "goodplaces" -> advanced settings
** ".NET Framework" should be 4.0
** "Identity" should be NetworkService
* Install MVC4
** (On the IIS Manager page) Web Platform Install -> search MVC -> install MVC4
* Restart IIS
** Start Menu -> visual studio 2012 -> Visual Sutdio Tools -> Developer Command Prompt (as administrator)
** run "aspnet_regiis.exe -i"

SQL Server Configuration
------------------------
* Create a sql server "goodplaces"
* Add NETWORK SERVICE as a db_owner on good places:
** security -> Logins -> (right click) NETWORK_SERVICE -> login properties
** User Mapping ->  checkmark good places
** check the db_owner checkbox

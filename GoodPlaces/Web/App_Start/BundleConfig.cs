﻿using System.Web;
using System.Web.Optimization;

namespace GoodPlaces
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/js").Include(
				"~/Scripts/jquery-1.*",
				"~/Scripts/bootstrap.js",
				"~/Scripts/jquery.validate.js",
				"~/scripts/jquery.validate.unobtrusive.js",
				"~/Scripts/jquery.validate.unobtrusive-custom-for-bootstrap.js"
				));
			
			bundles.Add(new StyleBundle("~/content/styles/bundle-core").Include(
				"~/Content/bootstrap.css",
				"~/Content/css/base.css",
				"~/Content/css/layout.css",
				"~/Content/css/core.css"
				));

			bundles.Add(new StyleBundle("~/content/styles/bundle-homepage").Include(
				"~/Content/bootstrap.css",
				"~/Content/css/base.css",
				"~/Content/css/homepage.css"
				));
		}
	}
}
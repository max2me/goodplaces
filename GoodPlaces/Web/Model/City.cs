﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
    public class City : Entity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual USState USState { get; set; }

        public virtual string NiceCityState
        {
            get { return this.Name + ", " + USState.Name; }
        }
    }
}
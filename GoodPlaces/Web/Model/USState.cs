﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
    public class USState : Entity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string LongName { get; set; }
    }
}
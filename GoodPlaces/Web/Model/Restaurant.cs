﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class Restaurant : Entity
    {
        public virtual string Name { get; set; }
        public virtual string VenueId { get; set; }
        public virtual string Description { get; set; }
        public virtual string Phone { get; set; }
        public virtual string FormattedPhone { get; set; }
        public virtual string Homepage { get; set; }
        public virtual string FoursquareUrl { get; set; }
        public virtual string Categories { get; set; }
        public virtual string Tags { get; set; }
        public virtual string Twitter { get; set; }
        public virtual Foursquare Foursquare { get; set; }
		public virtual Location Location { get; set; }
        public virtual City City { get; set; }

        public virtual string NiceCityState {
            get { return Location.City + ", " + Location.State; }
        }
    }

}
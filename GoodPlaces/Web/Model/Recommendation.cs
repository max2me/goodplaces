﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class Recommendation : Entity
	{
		public virtual bool GoodForBreakfast { get; set; }
		public virtual bool GoodForBrunch { get; set; }
		public virtual bool GoodForLunch { get; set; }
		public virtual bool GoodForDinner { get; set; }
		public virtual bool GoodForHappyHour { get; set; }
		public virtual Restaurant Restaurant { get; set; }
		public virtual DateTime Added { get; set; }
	}
}
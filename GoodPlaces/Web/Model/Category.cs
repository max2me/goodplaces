﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class Category
	{
		public virtual long Id { get; set; }
		public virtual string Name { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class CachedFacebookQuery : Entity
	{
		public virtual string Query { get; set; }
		public virtual string AccessToken { get; set; }
		public virtual string Response { get; set; }
	}
}
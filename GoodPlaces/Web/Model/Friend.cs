﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class Friend
	{
		public virtual long Id { get; set; }
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string Photo { get; set; }
	}
}
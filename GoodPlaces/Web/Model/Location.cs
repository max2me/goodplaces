﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class Location
	{
		public virtual string Street { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string Zip { get; set; }
		public virtual string Country { get; set; }
		public virtual decimal Latitude { get; set; }
		public virtual decimal Longitude { get; set; }
	}
}
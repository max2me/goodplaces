﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public class User : Entity
	{
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string Email { get; set; }
        public virtual string Gender { get; set; }
        public virtual string ImageUrl { get; set; }
		public virtual long FacebookId { get; set; }
		public virtual string FacebookAccessToken { get; set; }
		public virtual Iesi.Collections.Generic.ISet<Friend> Friends { get; protected set; }
		public virtual Iesi.Collections.Generic.ISet<Recommendation> Recommendations { get; protected set; }
		public virtual bool CompletedSetup { get; set; }
		public virtual bool SyncedFacebookData { get; set; }

		public virtual string FullName
		{
			get { return FirstName + " " + LastName; }
		}

        // returns true if the user has favorited the restaurant guid passed.
        public virtual bool FavoritedRestaurant(Restaurant restaurant)
        {
            foreach (Recommendation r in Recommendations) {
                if(r.Restaurant == restaurant) {
                    return true;
                }
            }
            return false;
        }


		public User()
		{
			Friends = new Iesi.Collections.Generic.HashedSet<Friend>();
			Recommendations = new Iesi.Collections.Generic.HashedSet<Recommendation>();
		}
	}
}
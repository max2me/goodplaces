﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model
{
	public abstract class Entity : EqualityAndHashCodeProvider<Entity, Guid>
	{
		public override Guid Id { get; set; }
	}
}
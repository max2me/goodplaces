﻿using Iesi.Collections.Generic;

namespace GoodPlaces.Model
{
	public class Page
	{
		public Page()
		{
			Fans = new HashedSet<Friend>();
			Location = new Location();
			Specialties = new Specialties();
			Categories = new HashedSet<Category>();
		}

		public virtual long Id { get; set; }
		public virtual string Name { get; set; }
		public virtual string Phone { get; set; }
		public virtual string About { get; set; }
		public virtual Location Location { get; set; }
		public virtual Specialties Specialties { get; set; }
		public virtual ISet<Friend> Fans { get; set; }
		public virtual ISet<Category> Categories { get; set; }

		protected bool Equals(Page other)
		{
			return Id == other.Id;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Page) obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
	}


	public class Specialties
	{
		public virtual bool Breakfast { get; set; }
		public virtual bool Lunch { get; set; }
		public virtual bool Dinner { get; set; }
		public virtual bool Coffee { get; set; }
		public virtual bool Drinks { get; set; }
	}
}
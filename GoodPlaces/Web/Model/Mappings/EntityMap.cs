﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public abstract class EntityMap<T> : ClassMap<T> where T : Entity
	{
		protected EntityMap()
		{
			Id(x => x.Id).GeneratedBy.GuidComb();
		}
	}
}
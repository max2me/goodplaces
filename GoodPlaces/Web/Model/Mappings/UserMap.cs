﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class UserMap : EntityMap<User>
	{
		public UserMap()
		{
			Map(x => x.FirstName);
			Map(x => x.LastName);
			Map(x => x.Email);
            Map(x => x.Gender);
			Map(x => x.FacebookAccessToken);
			Map(x => x.ImageUrl);
			Map(x => x.FacebookId).Index("IX_User_FacebookId");

			HasManyToMany(x => x.Friends)
				.Cascade.DeleteOrphan()
				.Table("UserFriend")
				.ParentKeyColumn("Usser")
				.ChildKeyColumn("Friend")
				.LazyLoad();

			HasMany(x => x.Recommendations)
				.Cascade.All()
				.LazyLoad();

			Map(x => x.CompletedSetup);
			Map(x => x.SyncedFacebookData);
		}
	}
}
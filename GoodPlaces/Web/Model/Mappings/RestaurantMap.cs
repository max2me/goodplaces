﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class RestaurantMap : EntityMap<Restaurant>
    {
        public RestaurantMap() : base()
        {
            Map(x => x.Name);
            Map(x => x.VenueId).Length(50).Unique().Index("IX_RestaurantVenueId");
	        Map(x => x.Description).Length(1000);
            Map(x => x.Phone);
            Map(x => x.FormattedPhone);
            Map(x => x.Homepage);
            Map(x => x.FoursquareUrl);
            Map(x => x.Categories);
            Map(x => x.Tags);
            Map(x => x.Twitter);
			Component(x => x.Location).ColumnPrefix("Location_");
            Component(x => x.Foursquare).ColumnPrefix("FS_");
            References(x => x.City);
        }
    }
}
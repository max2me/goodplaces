﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class PageMap : ClassMap<Page>
	{
		public PageMap()
		{
			Id(x => x.Id).GeneratedBy.Assigned(); ;
			Map(x => x.Name);
			Map(x => x.Phone);
			Map(x => x.About).Length(500);

			Component(x => x.Location).ColumnPrefix("Location_");
			Component(x => x.Specialties).ColumnPrefix("Specialties_");

			HasManyToMany(x => x.Fans)
				 .Cascade.DeleteOrphan()
				 .Table("PageFriend")
				 .LazyLoad();

			HasManyToMany(x => x.Categories)
				.Cascade.DeleteOrphan()
				.Table("PageCategory")
				.LazyLoad();
		}
	}
}
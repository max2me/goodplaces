﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class FriendMap: ClassMap<Friend>
	{
		public FriendMap(): base()
		{
			Id(x => x.Id).GeneratedBy.Assigned(); ;
			Map(x => x.FirstName);
			Map(x => x.LastName);
			Map(x => x.Photo);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
    public class FoursquareMap : ComponentMap<Foursquare>
    {
        public FoursquareMap()
            : base()
        {
            Map(x => x.CheckIns);
            Map(x => x.UsersCount);
            Map(x => x.Likes);
        }
    }
}
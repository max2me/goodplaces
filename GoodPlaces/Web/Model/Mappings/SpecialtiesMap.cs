﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class SpecialtiesMap: ComponentMap<Specialties>
	{
		public SpecialtiesMap()
		{
			Map(x => x.Breakfast);
			Map(x => x.Lunch);
			Map(x => x.Dinner);
			Map(x => x.Coffee);
			Map(x => x.Drinks);
		}
	}
}
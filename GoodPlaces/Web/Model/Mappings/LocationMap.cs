﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class LocationMap : ComponentMap<Location>
	{
		public LocationMap()
		{
			Map(x => x.Street);
			Map(x => x.City);
			Map(x => x.State);
			Map(x => x.Zip);
			Map(x => x.Country);
			Map(x => x.Latitude);
			Map(x => x.Longitude);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model.Mappings
{
	public class RecommendationMap : EntityMap<Recommendation>
	{
		public RecommendationMap()
		{
			Map(x => x.GoodForBreakfast);
			Map(x => x.GoodForBrunch);
			Map(x => x.GoodForDinner);
			Map(x => x.GoodForHappyHour);
			Map(x => x.GoodForLunch);
			Map(x => x.Added);
			References(x => x.Restaurant)
				.ForeignKey("FK_RecommendationToRestaurant")
				.Cascade.All();

		}
	}
}
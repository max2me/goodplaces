﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model.Mappings
{
    public class CityMap : EntityMap<City>
    {
        public CityMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            References(x => x.USState);
        }
    }
}
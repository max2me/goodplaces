﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class CachedFacebookQueryMap : EntityMap<CachedFacebookQuery>
	{
		public CachedFacebookQueryMap(): base()
		{
			Map(x => x.Query).CustomSqlType("nvarchar(4000)").Length(4000);
			Map(x => x.AccessToken).Index("IX_CachedFacebookQueryMap_Query");
			Map(x => x.Response).CustomSqlType("ntext").Length(Int32.MaxValue);
		}
	}
}
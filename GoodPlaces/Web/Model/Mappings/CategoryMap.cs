﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace GoodPlaces.Model.Mappings
{
	public class CategoryMap : ClassMap<Category>
	{
		public CategoryMap()
		{
			Id(x => x.Id).GeneratedBy.Assigned();
			Map(x => x.Name);
		}
	}
}
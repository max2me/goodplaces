﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.Model.Mappings
{
    public class USStateMap : EntityMap<USState>
    {
        public USStateMap()
		{
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.LongName);
		}
    }
}
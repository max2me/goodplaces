﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.ViewModels.Places
{
	public class Category
	{
		private List<Like> _places;
		private string _name;
		public long Id { get; set; }


		public string Name
		{
			get { return _name; }
			set
			{
				if (value != "Restaurant")
					_name = value.Replace("Restaurant", "");
				else
					_name = value + "s";
			}
		}

		public List<Like> Places
		{
			get { return _places ?? (_places = new List<Like>()); }
		}

		#region Equality members

		protected bool Equals(Category other)
		{
			return Id == other.Id;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((Category)obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		#endregion
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using GoodPlaces.ViewModels.Core;
using GoodPlaces.App.Services;
using NHibernate;
using NHibernate.Linq;

namespace GoodPlaces.ViewModels.Places
{
    public class DetailsViewModel : BaseViewModel
    {
        private readonly User _user;
		private readonly ISession _session;
        private readonly RestaurantService _resService;
		//public Implicit Implicit { get; private set; }
		//public Recommendations Recommendations { get; private set; }
        public Restaurant restaurant { get; private set; }
        public int favoriteCount { get; private set; }
        public int beenThereCount { get; private set; }
        public float battingAverage { get; private set; }

        public DetailsViewModel(Restaurant res, User user, ISession session): base()
		{
			_user = user;
			_session = session;
            _resService = new RestaurantService(session);
            restaurant = res;
            favoriteCount = _resService.GetFavoriteCount(res.Id);
            battingAverage = _resService.GetBattingAverage(res.Id);
            beenThereCount = _resService.GetUsersCount(res.Id);
			//Implicit = new Implicit(_user, _session);
			//Recommendations = new Recommendations(_user, _session);
		}

    }
}
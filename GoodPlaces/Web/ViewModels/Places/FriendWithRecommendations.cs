﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.ViewModels.Places
{
	public class FriendWithRecommendations
	{
		public string Name { get; set; }
		public string Avatar { get; set; }
		public List<Recommendation> Recommendations { get; set; } 
	}
}
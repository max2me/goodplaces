﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodPlaces.Model;
using NHibernate;
using NHibernate.Linq;

namespace GoodPlaces.ViewModels.Places
{
	public class Implicit
	{
		private readonly ISession _session;
		private readonly User _user;

		public Implicit(User user, ISession session)
		{
			_user = user;
			_session = session;

			Cities = ComputeCities();
		}

		public List<City> Cities { get; set; }

		private List<City> ComputeCities()
		{
			var cityMap = new Dictionary<string, City>();
			IQueryable<Page> pages =
				_session.Query<Page>()
				        .Where(x => x.Fans.Any(f => _user.Friends.Contains(f)))
				        .Where(p => p.Location.Street != null && p.Location.City != null);

			foreach (Page page in pages)
			{
				string cityName = GetCityName(page);

				City city = cityMap.ContainsKey(cityName) ? cityMap[cityName] : null;
				if (city == null)
				{
					city = new City {Name = cityName};
					cityMap.Add(cityName, city);
				}

				city.AddLike(GetLike(page));
			}

			return cityMap.Keys.Select(k => cityMap[k]).OrderByDescending(c => c.PlacesCount).ToList();
		}

		private Like GetLike(Page page)
		{
			var like = new Like {Page = page};

			foreach (Friend friend in page.Fans.Where(f => _user.Friends.Contains(f)))
				like.Fans.Add(friend);
			return like;
		}

		private static string GetCityName(Page page)
		{
			string cityName;
			if (String.IsNullOrEmpty(page.Location.State))
				cityName = String.Format("{0}, {1}", page.Location.City, page.Location.Country);
			else
				cityName = String.Format("{0}, {1}", page.Location.City, page.Location.State);
			return cityName;
		}
	}


	
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace GoodPlaces.ViewModels.Places
{
	public class Recommendation
	{
		public string Name { get; set; }
		public Guid Id { get; set; }
	}
}
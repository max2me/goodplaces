﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;

namespace GoodPlaces.ViewModels.Places
{
	public class Like
	{
		public Like()
		{
			Fans = new List<Friend>();
		}

		public string ShorterName
		{
			get
			{
				if (Page.Name.Length > 30)
					return Page.Name.Substring(0, 30).Trim() + "...";

				return Page.Name;
			}
		}

		public string MiniCategory { get; set; }
		public bool HasMiniCategory
		{
			get { return !String.IsNullOrEmpty(MiniCategory); }
		}

		public Page Page { get; set; }
		public List<Friend> Fans { get; set; }

		protected bool Equals(Like other)
		{
			return Equals(Page, other.Page);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Like)obj);
		}

		public override int GetHashCode()
		{
			return (Page != null ? Page.GetHashCode() : 0);
		}
	}
}
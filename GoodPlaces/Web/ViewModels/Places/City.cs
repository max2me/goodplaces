﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.ViewModels.Places
{

	public class City
	{
		private readonly List<Like> _places;
		static readonly long[] IgnoreCategories = new[] { 273819889375819 /* Restaurant */, 109527622457518 /* Shopping mall*/ };

		private List<Category> _categories;

		public City()
		{
			_places = new List<Like>();
		}

		public string Name { get; set; }

		private int _placesCount;
		public int PlacesCount
		{
			get
			{
				if (_placesCount == 0)
				{
					foreach (var category in Categories)
						_placesCount += category.Places.Count;
				}

				return _placesCount;
			}
		}

		public List<Category> Categories
		{
			get { return _categories ?? (_categories = Categorize(_places)); }
		}

		private List<Category> Categorize(List<Like> places)
		{
			var categoriesMap = PlaceIntoCategories(places);
			var categories = DedupeCategories(categoriesMap);

			return categories;
		}

		private static List<Category> DedupeCategories(Dictionary<long, Category> categoriesMap)
		{
			var categories = categoriesMap.Values.OrderByDescending(c => c.Places.Count).ToList();

			var otherCategory = new Category() {Name = "Other"};

			// Sort categories by highest number of places
			// Starting with highest, iterate through removing previously found places
			for (var i = 0; i < categories.Count; i++)
			{
				var category = categories[i];

				foreach (var like in category.Places)
				{
					for (var j = i + 1; j < categories.Count; j++)
					{
						var cat2 = categories[j];
						cat2.Places.Remove(like);
					}
				}
			}

			foreach (var category in categories.Where(c => c.Places.Count == 1))
			{
				var place = category.Places.First();
				place.MiniCategory = category.Name;
				category.Places.Remove(place);

				otherCategory.Places.Add(place);
			}


			categories.RemoveAll(c => c.Places.Count == 0);
			var list = categories.OrderByDescending(c => c.Places.Count).ToList();
			
			if (otherCategory.Places.Count > 0)
				list.Add(otherCategory);

			if (list.Count == 1)
				list[0].Name = "";

			return list;
		}


		private Dictionary<long, Category> PlaceIntoCategories(IEnumerable<Like> places)
		{
			var result = new Dictionary<long, Category>();
			
			foreach (Like place in places)
			{
				if (place.Page.Categories.IsEmpty)
				{
					Category cat = null;

					if (result.ContainsKey(0))
					{
						cat = result[0];
					}
					else
					{
						cat = new Category();
						result.Add(cat.Id, cat);
					}

					cat.Places.Add(place);
				}

				foreach (var category in place.Page.Categories)
				{
					if (IgnoreCategories.Contains(category.Id) && place.Page.Categories.Count > 1) // TODO: Make it so place.Page.Category contains at least one non-ignored
						continue;

					var cat = GetCategory(result, category);

					cat.Places.Add(place);
				}
			}

			return result;
		}

		private static Category GetCategory(Dictionary<long, Category> categoriesMap, Model.Category category)
		{
			Category cat = null;

			if (categoriesMap.ContainsKey(category.Id))
			{
				cat = categoriesMap[category.Id];
			}
			else
			{
				cat = new Category { Id = category.Id, Name = category.Name };
				categoriesMap.Add(cat.Id, cat);
			}
			return cat;
		}

		public void AddLike(Like like)
		{
			_places.Add(like);
		}
	}

}
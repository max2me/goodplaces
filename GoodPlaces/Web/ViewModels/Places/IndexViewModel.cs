﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using GoodPlaces.ViewModels.Core;
using NHibernate;
using NHibernate.Linq;

namespace GoodPlaces.ViewModels.Places
{
	public class IndexViewModel : BaseViewModel
	{
		private readonly User _user;
		private readonly ISession _session;
		public Implicit Implicit { get; private set; }
		public Recommendations Recommendations { get; private set; }
		
		public IndexViewModel(User user, ISession session):base(user)
		{
			_user = user;
			_session = session;

			Implicit = new Implicit(_user, _session);
			Recommendations = new Recommendations(_user, _session);
		}
	}
}
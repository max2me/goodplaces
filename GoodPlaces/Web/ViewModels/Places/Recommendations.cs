﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using NHibernate;
using NHibernate.Linq;

namespace GoodPlaces.ViewModels.Places
{
	public class Recommendations
	{
		public bool HasAny
		{
			get { return Friends != null && Friends.Count > 0; }
		}
		
		public List<FriendWithRecommendations> Friends { get; set; }

		public Recommendations(User user, ISession session)
		{
			var facebookIds = user.Friends.Select(f => f.Id).ToList();
			var friends = session.Query<User>().Where(u => facebookIds.Contains(u.FacebookId)).ToList();
			
			Friends = new List<FriendWithRecommendations>();
			foreach (var f in friends)
			{
				Friends.Add(new FriendWithRecommendations
				{
					Name = f.FullName,
					Avatar = String.Format("//graph.facebook.com/{0}/picture", f.FacebookId),
					Recommendations = f.Recommendations.ToList().ConvertAll(r => new Recommendation
					{
						Name = r.Restaurant.Name,
						Id = r.Restaurant.Id,

					})
				});
			}
		}
	}
}
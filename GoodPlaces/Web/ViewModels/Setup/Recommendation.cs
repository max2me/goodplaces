﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using FluentValidation.Attributes;
using GoodPlaces.ViewModels.Core;

namespace GoodPlaces.ViewModels.Setup
{
	public enum RecommendationType
	{
		Breakfast,
		Lunch,
		Dinner
	}

	[Validator(typeof(Recommendation.Metadata))]
	public class Recommendation
	{
		public string Name { get; set; }
		public string VenueId  { get; set; }
		public RecommendationType Type { get; set;}
		public string TypeName
		{
			get { return Type.ToString().ToLower(); }
		}

		public class Metadata : ViewModelMetadata<Recommendation>
		{
			public Metadata()
			{
				CascadeMode = CascadeMode.StopOnFirstFailure;

				RuleFor(x => x.Name)
					.NotEmpty();
			}
		}
	}
}
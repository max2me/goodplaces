﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using GoodPlaces.ViewModels.Core;

namespace GoodPlaces.ViewModels.Setup
{
	public class IndexViewModel : BaseViewModel
	{
		public List<Recommendation> Recommendations { get; set; }

		public IndexViewModel()
		{
		}

		public IndexViewModel(User currentUser) : base(currentUser)
		{
			Recommendations = new List<Recommendation>()
				                  {
					                  new Recommendation() { Type = RecommendationType.Breakfast},
					                  new Recommendation() { Type = RecommendationType.Lunch},
					                  new Recommendation() { Type = RecommendationType.Dinner},
				                  };
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;

namespace GoodPlaces.ViewModels.Core
{
	public abstract class ViewModelMetadata<T> : AbstractValidator<T>
	{
		protected ViewModelMetadata()
		{
			CascadeMode = CascadeMode.StopOnFirstFailure;
		}
	}
}
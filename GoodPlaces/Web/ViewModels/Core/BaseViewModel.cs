﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;

namespace GoodPlaces.ViewModels.Core
{
	public class BaseViewModel
	{
		public User CurrentUser { get; private set; }

		public BaseViewModel()
		{
		}

		public BaseViewModel(User currentUser)
		{
			CurrentUser = currentUser;
		}
	}
}
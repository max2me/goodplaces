﻿var ll = {
	lat: 0,
	lon: 0
};

var gp = {};


$(function () {
	detectLocation();
	$('.place-name').eq(0).focus();
});

function setupAutocomplete() {
	$('.place-name').autocomplete({
		minLength: 3,
		source: function(request, response) {
			$.ajax({
				url: '/Services/GetVenuesByCity/',
				dataType: 'json',
				data: {
					lat: ll.lat,
					lon: ll.lon,
					query: request.term
				},
				success: function (data) {
					if (data.length > 10)
						data = data.slice(0, 10);
					
					response(data);
				},

				error: function() {
					response([]);
				}
			});
		},
		select: function(event, ui) {
			var nameBox = $(this);

			nameBox
				.val(ui.item.label)
				.parents('.place')
				.find('.place-venueid')
				.val(ui.item.id);

			return false;
		}
	});
}

function getCityFavorite(city, state) {
    $('#select').text('Michou seems popular.');
}

function renderCityModule() {
    $('#select').html(function (idx, html) {
        return '<a href="#">Select a city</a>';
    });
}

function handleCityError() {
    $('#select').text('Mmmmm, the choices.');
}

function getCity(lat, lon) {
    $.ajax({
        url: '/Services/GetPlaceInfo/',
        dataType: 'json',
        data: {
            lat: ll.lat,
            lon: ll.lon
        },
        success: function (data) {
            if (data.placename && data.adminCode1) {
                gp.city = data.placename;
                gp.state = data.adminCode1;
                $('#city').text(gp.city + '...');
                getCityFavorite();
            } else {
                handleCityError();
            }
        },
        error: function () {
            handleCityError();
        }
    });
}

function handleNoGeo() {
    renderCityModule();
}

function detectLocation() {
	if (!('geolocation' in navigator))
	{
	    handleNoGeo();
		return;
	}

	navigator.geolocation.getCurrentPosition(function(pos) {
		ll.lat = pos.coords.latitude;
		ll.lon = pos.coords.longitude;

		window.setTimeout(function () {
		    getCity(ll.lat, ll.lon);
		}, 50);

		setupAutocomplete();
	}, function(error) {
	    handleNoGeo();
	});
}
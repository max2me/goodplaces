﻿using System.Web.Mvc;

using NHibernate;

using Ninject;
using Ninject.Syntax;

namespace GoodPlaces.Filters
{
	public class InTransactionActionFilter : IActionFilter
	{
		private readonly IResolutionRoot _root;

		public InTransactionActionFilter(IResolutionRoot root)
		{
			_root = root;
		}

		public void OnActionExecuting(ActionExecutingContext filterContext)
		{
			_root.Get<ISession>().BeginTransaction();
		}

		public void OnActionExecuted(ActionExecutedContext filterContext)
		{
			_root.Get<ISession>().Transaction.Commit();
		}
	}
}
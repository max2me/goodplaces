﻿using System;
using System.Web.Mvc;

using GoodPlaces.Filters;

using Ninject.Modules;
using Ninject.Web.Mvc.FilterBindingSyntax;

namespace GoodPlaces.Modules
{
	public class InTransactionAttribute : Attribute
	{
	}

	public class InTransactionModule : NinjectModule
	{
		public override void Load()
		{
			this.BindFilter<InTransactionActionFilter>(FilterScope.Action, 0)
				.WhenActionMethodHas<InTransactionAttribute>()
				.InTransientScope();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;

namespace GoodPlaces.App.Modules
{
	public class BindingModule : NinjectModule
	{
		public override void Load()
		{
			Bind<GpGlobal>().ToSelf().InSingletonScope();
			
		}
	}
}
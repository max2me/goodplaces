﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Data;
using GoodPlaces.Model;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Ninject.Web.Common;

namespace GoodPlaces.App.Modules
{
	public class DatabaseConfiguration : NinjectModule
	{
		public override void Load()
		{
			Bind<ISessionFactory>().ToConstant(BuildFactory()).InSingletonScope();
			Bind<ISession>().ToMethod(OpenSession).InRequestScope();
			Bind<IStatelessSession>().ToMethod(OpenStatelessSession).InRequestScope();
		}

		private static IStatelessSession OpenStatelessSession(IContext context)
		{
			return context.Kernel.Get<ISessionFactory>().OpenStatelessSession();
		}

		private static ISession OpenSession(IContext context)
		{
			return context.Kernel.Get<ISessionFactory>().OpenSession();
		}

		private static ISessionFactory BuildFactory()
		{
			return Fluently.Configure()
				.Database(MsSqlConfiguration.MsSql2008.ConnectionString(builder => builder.FromConnectionStringWithKey("GoodPlaces")))
				.Mappings(FluentMappingsContainer)
				.ExposeConfiguration(Update)
				.BuildSessionFactory();
		}

		private static void Update(Configuration configuration)
		{
			new SchemaUpdate(configuration).Execute(true, true); 
		}

		private static void Export(Configuration configuration)
		{
			new SchemaExport(configuration)
			            .SetOutputFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\schema.sql"))
			            .Create(false, false);
		}

		private static void FluentMappingsContainer(MappingConfiguration configuration)
		{
			FluentMappingsContainer container = configuration.FluentMappings.AddFromAssemblyOf<User>();

			//#if DEBUG
			//    container.ExportTo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\Mappings"));
			//#endif
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using GoodPlaces.App.Services.FacebookModel;
using GoodPlaces.Model;
using GoodPlaces.Utilities;
using NHibernate;
using NHibernate.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject.Activation;
using RestSharp;
using RestSharp.Contrib;
using User = GoodPlaces.App.Services.FacebookModel.User;

namespace GoodPlaces.App.Services
{
	public class FacebookService
	{
		private readonly ISession _session;
        private const string openGraphRoot = "https://graph.facebook.com/";
		public Dictionary<long, User> _users = new Dictionary<long, User>();

		public FacebookService(ISession session)
		{
			_session = session;
		}

		public List<User> GetFriends(string token)
		{
			var response = ExecuteQuery("SELECT first_name, last_name, name, uid, pic_square, username FROM user WHERE uid  IN (SELECT uid2 FROM friend WHERE uid1 = me())", token);
			var friends = JsonConvert.DeserializeObject<Response<User>>(response);

			return friends.data;
		}
		
		public IEnumerable<RestaurantPage> GetRestaurantPagesAndInfo(string token)
		{
			var response = ExecuteQuery(JsonConvert.SerializeObject(new PagesQuery()), token);

			var data = JsonConvert.DeserializeObject<Response<BulkQuery>>(response);

			var distilled = data.data.Single(x => x.name == "distilled").fql_result_set.ConvertAll(x => x.ToObject<FanPageRelationship>());
			var pages = data.data.Single(x => x.name == "pages").fql_result_set.ConvertAll(x => x.ToObject<RestaurantPage>());

			foreach (var page in pages)
			{
				page.fans = distilled.Where(x => x.page_id == page.page_id).Select(x => x.uid).ToList();
			}

			return pages;
		}

		public Me GetMe(string token)
		{
			var client = new RestClient(FacebookSettings.AccessPoint);
			var request = new RestRequest("me", Method.GET);
			request.AddParameter("access_token", token);

			var response = client.Execute(request);

			var user = JsonConvert.DeserializeObject<Me>(response.Content);
		
			return user;
		}

		public string GetAccessToken(string code, string redirectUrl)
		{
			var client = new RestClient(FacebookSettings.AccessPoint);
			var request = new RestRequest("/oauth/access_token", Method.GET);
			request.AddParameter("client_id", FacebookSettings.AppID);
			request.AddParameter("client_secret", FacebookSettings.AppSecret);
			request.AddParameter("code", code);
			request.AddParameter("redirect_uri", redirectUrl);

			var response = client.Execute(request);

			var content = HttpUtility.ParseQueryString(response.Content);
			var token = content["access_token"];
			return token;
		}

		private string ExecuteQuery(string query, string token)
		{
			CachedFacebookQuery cachedQ = _session.Query<CachedFacebookQuery>().FirstOrDefault(x => x.Query == query && x.AccessToken == token);
			if (cachedQ == null)
				return QueryFacebook(query, token);

			return cachedQ.Response;
		}

		private string QueryFacebook(string query, string token)
		{
			var client = new RestClient(FacebookSettings.AccessPoint);
			var request = new RestRequest("fql", Method.GET);
			request.AddParameter("access_token", token);
			request.AddParameter("q", query);

			string response = client.Execute(request).Content;

			var cachedQ = new CachedFacebookQuery
				              {
					              Query = query,
					              Response = response,
								  AccessToken = token
				              };

			_session.Save(cachedQ);
			_session.Flush();

			return response;
		}

        private static JObject OpenGraphQuery(string objectId, string fields, string accessToken)
        {
            var client = new RestClient(openGraphRoot);
            var request = new RestRequest(objectId);
            request.AddParameter("access_token", accessToken);
            request.AddParameter("fields", fields);
            var response = client.Execute(request);
            return JsonConvert.DeserializeObject<JObject>(response.Content);
        }

		public string GetRedirectUrl(string absoluteUri)
		{
			return String.Format("https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}&scope=friends_likes,email",
							  FacebookSettings.AppID, HttpUtility.UrlEncode(absoluteUri));
		}

        public static string GetProfileImage(string accessToken)
        {
            JObject jsonObject = OpenGraphQuery("me", "picture.height(400).width(400)", accessToken);
            if (jsonObject["error"] == null)
            {
                return jsonObject["picture"]["data"]["url"].ToString();
            }
            else
            {
                // some error validation is required.
                return null;
            }
        }

	}
}
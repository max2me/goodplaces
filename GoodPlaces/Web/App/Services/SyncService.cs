﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using NHibernate;

namespace GoodPlaces.App.Services
{
	public class SyncService
	{
		private readonly ISession _session;
		private readonly FacebookService _facebookService;

		public SyncService(ISession session, FacebookService facebookService)
		{
			_session = session;
			_facebookService = facebookService;
		}

		public void SyncFriends(User user)
		{
			var friends = _facebookService.GetFriends(user.FacebookAccessToken);

			user.Friends.Clear();
			_session.Update(user);

			foreach (var f in friends)
			{
				var uid = long.Parse(f.uid);
				var friend = _session.Get<Friend>(uid);
				if (friend == null)
				{
					friend = new Friend()
						         {
							         FirstName = f.first_name,
							         LastName = f.last_name,
							         Id = long.Parse(f.uid),
							         Photo = f.pic_square
						         };

					_session.SaveOrUpdate(friend);
				}

				
				user.Friends.Add(friend);
			}

			_session.Update(user);
			_session.Flush();
		}

		public void SyncPages(User user)
		{
			var pages = _facebookService.GetRestaurantPagesAndInfo(user.FacebookAccessToken);

			foreach (var p in pages)
			{
				var page = new Page()
					            {
						            Id = p.page_id,
						            Name = p.name,
						            Phone = p.phone,
						            About = p.about,
						            Specialties = new Specialties
							                          {
								                          Breakfast = p.restaurant_specialties.breakfast,
								                          Coffee = p.restaurant_specialties.coffee,
								                          Dinner = p.restaurant_specialties.dinner,
								                          Drinks = p.restaurant_specialties.drinks,
								                          Lunch = p.restaurant_specialties.lunch,
							                          },
						            Location = new Location
							                       {	
													   Street = p.location.street,
													   City = p.location.city,
													   Country = p.location.country,
													   State = p.location.state,
													   Latitude = p.location.latitude,
													   Longitude = p.location.longitude,
													   Zip = p.location.zip
							                       }
					            };


				foreach (var f in p.fans)
				{
					var fan = _session.Get<Friend>(f);

					if (!page.Fans.Contains(fan))
						page.Fans.Add(fan);
				}

				foreach (var c in p.categories)
				{
					var category = _session.Get<Category>(c.id);
					if (category == null)
					{
						category = new Category() { Id = c.id, Name = c.name};
						_session.Save(category);
					}

					page.Categories.Add(category);
				}

				_session.SaveOrUpdate(page);
			}

			_session.Flush();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FoursquareModel
{
    public class VenueCategories
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
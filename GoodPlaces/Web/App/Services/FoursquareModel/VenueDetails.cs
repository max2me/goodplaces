﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FoursquareModel
{
	/// <summary>
	/// Represents full venue details
	/// </summary>
	public class VenueDetails
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
        public Contact Contact { get; set; }
        public string Url { get; set; }
        public string ShortUrl { get; set; }
        public string[] Tags { get; set; }
        public VenueCategories[] Categories { get; set; }
		public Location Location { get; set; }
	}
}
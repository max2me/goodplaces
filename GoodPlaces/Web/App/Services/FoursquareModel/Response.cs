﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace GoodPlaces.App.Services.FoursquareModel
{
    public class Response<T>
    {
        public JsonObject meta { get; set; }
        public T response { get; set; }
    }
}
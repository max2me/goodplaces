﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FoursquareModel
{
    public class VenueOverview
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Location Location { get; set; }
    }
}
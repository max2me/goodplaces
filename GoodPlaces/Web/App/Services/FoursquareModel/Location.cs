﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FoursquareModel
{
    public class Location
    {
        public string Address { get; set; }
        public float Lat { get; set; }
        public float Lng { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}
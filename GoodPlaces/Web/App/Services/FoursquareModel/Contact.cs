﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FoursquareModel
{
    public class Contact
    {
        public string Phone { get; set; }
        public string FormattedPhone { get; set; }
        public string Twitter { get; set; }
    }
}
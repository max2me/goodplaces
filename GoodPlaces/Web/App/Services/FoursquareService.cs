﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.App.Services.FoursquareModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GoodPlaces.App.Services
{
    public class FoursquareService
    {
        public const string ApiPath = "https://api.foursquare.com/v2/";
        public const string VenuePath = "venues/";
        public const string Version = "20130202";
        public const string ClientId = "GXLG3JEHHFIBJYWHZVANTZO5KVTQTZMRX3Q5VAESMZ4OQEJ5";
        public const string ClientSecret = "PRN2YL0KNSBWEVJQPNH1J43IKITE2BHR0ETIDSV2VF5TPI5Q";

        public static VenueDetails GetVenueDetails(String venueId)
        {
            var client = new RestClient(ApiPath);
            var request = new RestRequest(VenuePath + venueId);
            request.AddParameter("client_id", ClientId);
            request.AddParameter("client_secret", ClientSecret);
            var response = client.Execute(request);
            var s = JsonConvert.DeserializeObject<JObject>(response.Content)["response"]["venue"].ToString();
            return JsonConvert.DeserializeObject<VenueDetails>(s);
        }

        public static List<VenueOverview> GetVenuesByCity(double lat, double lon, string query)
        {
            var client = new RestClient(ApiPath);
            var request = new RestRequest(VenuePath + "search");
            request.AddParameter("client_id", ClientId);
            request.AddParameter("client_secret", ClientSecret);
            request.AddParameter("ll", lat + "," + lon);
            request.AddParameter("radius", 17*1600);
            request.AddParameter("query", query);
            request.AddParameter("limit", "10");
            request.AddParameter("intent", "browse");
            request.AddParameter("categoryId", "4d4b7105d754a06374d81259");
            request.AddParameter("v", "20130201");
            var response = client.Execute(request);

            var s = JsonConvert.DeserializeObject<JObject>(response.Content)["response"]["venues"].ToString();
            return JsonConvert.DeserializeObject<List<VenueOverview>>(s);
        }
    }
}
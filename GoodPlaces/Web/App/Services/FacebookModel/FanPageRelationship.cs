﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class FanPageRelationship
	{
		public long page_id { get; set; }
		public long uid { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class Response<T>
	{
		public List<T> data { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class RestaurantSpecialties
	{
		public bool breakfast { get; set; }
		public bool lunch { get; set; }
		public bool dinner { get; set; }
		public bool coffee { get; set; }
		public bool drinks { get; set; }
	}
}
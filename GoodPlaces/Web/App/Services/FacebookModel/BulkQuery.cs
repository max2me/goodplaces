﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class BulkQuery
	{
		public string name { get; set; }
		public List<JObject> fql_result_set { get; set; }
	}
}
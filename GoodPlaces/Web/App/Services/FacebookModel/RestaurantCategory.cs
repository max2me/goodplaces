﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class RestaurantCategory
	{
		public long id { get; set; }
		public string name { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class PagesQuery
	{
		public string pages = "SELECT page_id, phone, about, name, location, restaurant_specialties, categories FROM page WHERE type = 'Restaurant/cafe' AND page_id IN " +
			"(SELECT page_id FROM page_fan WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()))";

		public string fanPages = "SELECT page_id, uid FROM page_fan WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";

		public string distilled = "SELECT page_id, uid FROM #fanPages WHERE page_id IN (SELECT page_id FROM #pages)";
	}

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class User
	{
		public string uid { get; set; }
		public string name { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string pic_square { get; set; }
	}
}
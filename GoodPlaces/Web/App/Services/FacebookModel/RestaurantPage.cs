﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodPlaces.App.Services.FacebookModel
{
	public class RestaurantPage
	{
		public long page_id { get; set; }
		public string name { get; set; }
		public string phone { get; set; }
		public string about { get; set; }
		public Location location { get; set; }
		public RestaurantSpecialties restaurant_specialties { get; set; }

		public List<long> fans { get; set; }
		public List<RestaurantCategory> categories { get; set; } 
	}
}
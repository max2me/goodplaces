﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using GoodPlaces.App.Services;
using GoodPlaces.App.Services.FoursquareModel;
using NHibernate;
using NHibernate.Linq;

namespace GoodPlaces.App.Services
{
    public class RestaurantService
    {
        private readonly ISession _session;

        public RestaurantService(ISession session)
		{
			_session = session;
		}

        public Restaurant GetRestaurantByVenueId(string id)
        {
            var restaurant = _session.Query<Restaurant>().SingleOrDefault(r => r.VenueId == id);

            if (restaurant == null)
            {
                var venue = FoursquareService.GetVenueDetails(id);
                var city = GetCityByName(venue.Location.City, venue.Location.State);
                var res = new Restaurant
                {
                    Name = venue.Name,
                    VenueId = venue.Id,
                    Phone = venue.Contact.Phone,
                    FormattedPhone = venue.Contact.FormattedPhone,
                    Homepage = venue.Url,
                    FoursquareUrl = venue.ShortUrl,
                    Categories = string.Join(",", venue.Categories.Select(x => x.Name)),
                    Tags = string.Join(",", venue.Tags),
                    Twitter = venue.Contact.Twitter,
                    Location = new Model.Location
                    {
                        Street = venue.Location.Address,
                        City = venue.Location.City,
                        State = venue.Location.State,
                        Zip = venue.Location.PostalCode,
                        Country = venue.Location.Country,
                        Latitude = (decimal)venue.Location.Lat,
                        Longitude = (decimal)venue.Location.Lng
                    },
                    City = city
                };
                _session.Save(res);
                _session.Flush();
                return res;
            }

            return restaurant;
        }

        public string[] GetCityStateDelimited()
        {
            //return string.Join(",", _session.Query<City>().Select(x => String.Format("{0}, {1}", x.Name, x.USState.Name).ToArray()));
            return _session.Query<City>().Select(x => String.Format("{0}, {1}", x.Name, x.USState.Name)).ToArray();
        }

        public City GetCityByName(string city, string state)
        {
            if (String.IsNullOrEmpty(city) || String.IsNullOrEmpty(state))
            {
                return null;
            }
            return _session.Query<City>().SingleOrDefault(x => x.Name.ToLower() == city.ToLower() && (x.USState.Name.ToLower() == state.ToLower() || x.USState.LongName.ToLower() == state.ToLower()));
        }

        public int GetFavoriteCount(Guid id)
        {
            //Guid resId = new Guid();
            //Guid.TryParse(id, out resId);
            /*if (resId == Guid.Empty)
            {
                return 0;
            }*/
            return _session.Query<Recommendation>().Count(c => c.Restaurant.Id == id);
        }

        public float GetBattingAverage(Guid id)
        {
            var restaurant = _session.Query<Restaurant>().SingleOrDefault(r => r.Id == id);
            int likes = GetLikes(id);
            int users = GetUsersCount(id);
            if (users == 0 || likes == 0)
            {
                return 0;
            }
            return likes / users;
        }

        public int GetCheckIns(Guid id)
        {
            var restaurant = _session.Query<Restaurant>().SingleOrDefault(r => r.Id == id);
            if (restaurant.Foursquare == null)
            {
                return 0;
            }
            return restaurant.Foursquare.CheckIns;
        }

        public int GetUsersCount(Guid id)
        {
            var restaurant = _session.Query<Restaurant>().SingleOrDefault(r => r.Id == id);
            if (restaurant.Foursquare == null)
            {
                return 0;
            }
            return restaurant.Foursquare.UsersCount;
        }

        public int GetLikes(Guid id)
        {
            var restaurant = _session.Query<Restaurant>().SingleOrDefault(r => r.Id == id);
            if (restaurant.Foursquare == null)
            {
                return 0;
            }
            return restaurant.Foursquare.Likes;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodPlaces.Model;
using NHibernate;
using Ninject;
using Ninject.Syntax;

namespace GoodPlaces.App
{
	public class GpGlobal
	{
		protected  readonly ISession _session;
		protected readonly IResolutionRoot _resolver;
	    private User _user;

		public GpGlobal(ISession session, IResolutionRoot resolver)
	    {
		    _session = session;
		    _resolver = resolver;
	    }

	    public User CurrentUser
		{
		    get
		    {
			    if (_user != null)
				    return _user;

			    var currentContext = _resolver.Get<HttpContextWrapper>();
			    if (currentContext.User == null || !currentContext.User.Identity.IsAuthenticated)
				    return null;

			    var name = currentContext.User.Identity.Name;

			    _user = _session.Get<User>(Guid.Parse(name));
			    return _user;
		    }
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace GoodPlaces.Utilities
{
	public class FacebookSettings
	{
		static FacebookSettings()
		{
			AppID = ConfigurationManager.AppSettings["FacebookSettings.AppID"];
			AppSecret = ConfigurationManager.AppSettings["FacebookSettings.AppSecret"];
		}

		public static string AppID { get; private set; }
		public static string AppSecret { get; private set; }

		public const string AccessPoint = "https://graph.facebook.com";
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using GoodPlaces.App.Services;
using GoodPlaces.App.Services.FoursquareModel;
using GoodPlaces.Model;
using GoodPlaces.Modules;
using GoodPlaces.ViewModels.Setup;
using NHibernate;
using NHibernate.Linq;
using Ninject.Syntax;
using Location = GoodPlaces.Model.Location;
using Recommendation = GoodPlaces.ViewModels.Setup.Recommendation;

namespace GoodPlaces.Controllers
{
    public class SetupController : BaseController
    {
		private readonly SyncService _syncService;
        private readonly RestaurantService _resService;

	    public SetupController(ISession session, IResolutionRoot resolver, SyncService syncService, RestaurantService resService) : base(session, resolver)
	    {
		    _syncService = syncService;
            _resService = resService;
	    }

	    public ActionResult Index()
        {
	        var model = new IndexViewModel(CurrentUser);
			
		    return View(model);
        }

	    public ActionResult SyncFacebookData()
	    {
		    if (CurrentUser.SyncedFacebookData)
			    return new EmptyResult();

			return ActualSyncFacebookData();
		}

	    public ActionResult ActualSyncFacebookData()
	    {
			_syncService.SyncFriends(CurrentUser);
			_syncService.SyncPages(CurrentUser);

			CurrentUser.SyncedFacebookData = true;
			_session.Save(CurrentUser);
			_session.Flush();

			return new EmptyResult();
	    }

	    [HttpPost]
		[InTransaction]
		public ActionResult Index(IndexViewModel model)
		{
			if (!ModelState.IsValid)
				return View(model);

			foreach (var recommendation in model.Recommendations)
			{
                var restaurant = _resService.GetRestaurantByVenueId(recommendation.VenueId);
				if (restaurant == null)
					continue; // TODO: Figure out how to handle custom venues

				var rec = new Model.Recommendation()
					                     {
						                     Added = DateTime.Now,
											 Restaurant = restaurant,
						                     GoodForBreakfast = recommendation.Type == RecommendationType.Breakfast,
						                     GoodForLunch = recommendation.Type == RecommendationType.Lunch,
						                     GoodForDinner = recommendation.Type == RecommendationType.Dinner,
					                     };
				
				CurrentUser.Recommendations.Add(rec);
			}

			CurrentUser.CompletedSetup = true;
			_session.SaveOrUpdate(CurrentUser);
			_session.Flush();

			return RedirectToAction("Index", "Places");
		}
    }
}

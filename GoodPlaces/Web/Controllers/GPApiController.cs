﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using Ninject.Syntax;
using GoodPlaces.Model;

namespace GoodPlaces.Controllers
{
    public class GPApiController : BaseController
    {
        //
        // GET: /Api/

        public GPApiController(ISession session, IResolutionRoot resolver) : base(session, resolver) { }

        public ActionResult Index()
        {
            return Json(new {}, JsonRequestBehavior.AllowGet);
        }

        // favorite a restaurant
        // Favorite?id=RESTAURANT_DB_ID
        public ActionResult Favorite(Guid id)
        {
            if(CurrentUser != null) {
                var restaurant = _session.Get<Restaurant>(id);
                if (restaurant != null) {
                    var rec = new Model.Recommendation()
                    {
                        Added = DateTime.Now,
                        Restaurant = _session.Get<Restaurant>(id)
                    };
                    var insert = true;
                    foreach (var r in CurrentUser.Recommendations)
                    {
                        if (r.Restaurant == restaurant)
                            insert = false;
                    }
                    if (insert)
                    {
                        CurrentUser.Recommendations.Add(rec);
                        _session.SaveOrUpdate(CurrentUser);
                        _session.Flush();
                    }
                    return Json("{'success':'true'}", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("{'success':'false'}", JsonRequestBehavior.AllowGet);
        }
    }
}

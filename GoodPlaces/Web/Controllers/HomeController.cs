﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using Ninject.Syntax;

namespace GoodPlaces.Controllers
{
    public class HomeController : BaseController
    {
	    public HomeController(ISession session, IResolutionRoot resolver) : base(session, resolver)
	    {
	    }

	    public ActionResult Index()
	    {
            if (CurrentUser == null)
            {
                return View();
            }

			if (CurrentUser.CompletedSetup)
				return RedirectToAction("Index", "Places");

			return RedirectToAction("Index", "Setup");
        }
    }
}

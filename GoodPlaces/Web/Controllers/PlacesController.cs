﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using GoodPlaces.Model;
using GoodPlaces.ViewModels.Places;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using Ninject.Syntax;

namespace GoodPlaces.Controllers
{
    public class PlacesController : BaseController
    {
		public PlacesController(ISession session, IResolutionRoot resolver) : base(session, resolver)
		{
		}

		public ActionResult Index()
        {
			if (!CurrentUser.CompletedSetup)
				return RedirectToAction("Index", "Setup");

			var model = new IndexViewModel(CurrentUser, _session);

			return View(model);
        }


		public ActionResult Details(Guid id)
		{
			var restaurant = _session.Get<Restaurant>(id);

			if (restaurant == null)
				return HttpNotFound();

            var model = new DetailsViewModel(restaurant, CurrentUser, _session);
            return View(model);
		}
    }
}

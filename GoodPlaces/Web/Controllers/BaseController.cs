﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodPlaces.Model;
using NHibernate;
using Ninject;
using Ninject.Syntax;

namespace GoodPlaces.Controllers
{
    public class BaseController : Controller
    {
		protected  readonly ISession _session;
		protected readonly IResolutionRoot _resolver;
	    private User _user;

	    public BaseController(ISession session, IResolutionRoot resolver)
	    {
		    _session = session;
		    _resolver = resolver;
            ViewBag.CurrentUser = this.CurrentUser;
            ViewBag.Style = "~/Content/styles/bundle-core";
            ViewBag.NoLogo = false;
	    }

	    protected User CurrentUser
		{
		    get
		    {
			    if (_user != null)
				    return _user;

			    var currentContext = _resolver.Get<HttpContextWrapper>();
			    if (currentContext.User == null || !currentContext.User.Identity.IsAuthenticated)
				    return null;

			    var name = currentContext.User.Identity.Name;

			    _user = _session.Get<User>(Guid.Parse(name));
			    return _user;
		    }
		}

    }
}

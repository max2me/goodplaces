﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodPlaces.App.Services;
using GoodPlaces.Model;
using NHibernate;
using Ninject.Syntax;

namespace GoodPlaces.Controllers
{
    public class ProfileController : BaseController
    {
        //
        // GET: /Profile/
		public ProfileController(ISession session, IResolutionRoot resolver) : base(session, resolver)
	    {
	    }

        public ActionResult Index(Nullable<Guid> id)
        {
            User user = null;
            if (id != null)
            {
                user = _session.Get<User>(id);
                if (user == null)
                    return HttpNotFound();
            }
            else
            {
                user = CurrentUser;
            }

            if (user == null)
                return Redirect("/");

            if (user.ImageUrl == null || user.ImageUrl.Equals(""))
            {
                user.ImageUrl = FacebookService.GetProfileImage(user.FacebookAccessToken);
                _session.Save(user);
                _session.Flush();
            }

	        return View(user);
        }

    }
}

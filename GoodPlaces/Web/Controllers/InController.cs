﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using GoodPlaces.App.Services;
using GoodPlaces.App.Services.FacebookModel;
using GoodPlaces.Utilities;
using NHibernate;
using NHibernate.Linq;
using User = GoodPlaces.Model.User;

namespace GoodPlaces.Controllers
{
	public class InController : Controller
	{
		private readonly FacebookService _facebookService;
		private readonly ISession _session;

		public InController(ISession session, FacebookService facebookService)
		{
			_session = session;
			_facebookService = facebookService;
		}

		public ActionResult Index()
		{
			return RedirectToAction("Index", "Home");
		}

		public ActionResult Bye()
		{
			FormsAuthentication.SignOut();

			return Redirect("/");
		}

		[HttpPost]
		public ActionResult Index(string connect, string returnUrl)
		{
#if OFFLINEDEBUG
			var user = _session.Query<User>().FirstOrDefault();
			if (user != null)
				return Authenticate(user, returnUrl);
#endif

			return RedirectToAction("Connect", new {returnUrl});
		}

		public ActionResult Connect(string code, string returnUrl)
		{
			var currentUrl = Request.Url.AbsoluteUri;
			var fbUrl = _facebookService.GetRedirectUrl(currentUrl);
	
			if (String.IsNullOrWhiteSpace(code))
				return Redirect(fbUrl);

			string token = _facebookService.GetAccessToken(code, currentUrl);

			if (String.IsNullOrEmpty(token))
				return RedirectToAction("Index", "Home", new {reason = "token_is_null"});

			var u = _facebookService.GetMe(token);
			if (u == null)
				return RedirectToAction("Index", "Home", new { reason = "user_is_null" });

			long uid = long.Parse(u.id);

			var user = _session.Query<User>().FirstOrDefault(x => x.FacebookId == uid);
			if (user == null)
			{
				user = new User
					       {
						       FacebookAccessToken = token,
						       FacebookId = uid,
						       FirstName = u.first_name,
						       LastName = u.last_name,
							   Email = u.email
					       };
                user.ImageUrl = FacebookService.GetProfileImage(token);
				_session.Save(user);
				_session.Flush();
			}

			return Authenticate(user, returnUrl);
		}

		private ActionResult Authenticate(User user, string returnUrl)
		{
			FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);

			if (String.IsNullOrWhiteSpace(returnUrl))
				return RedirectToAction("Index", "Places");

			return Redirect(returnUrl);
		}
	}
}
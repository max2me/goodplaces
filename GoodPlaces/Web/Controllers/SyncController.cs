﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using GoodPlaces.App.Services;
using GoodPlaces.Model;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Ninject;
using Ninject.Syntax;

namespace GoodPlaces.Controllers
{
	[Authorize]
    public class SyncController : BaseController
    {
	    private readonly SyncService _syncService;

		public SyncController(ISession session, IResolutionRoot resolver, SyncService syncService):base(session, resolver)
	    {
			_syncService = syncService;
	    }

		public ActionResult FacebookData()
		{
			_syncService.SyncFriends(CurrentUser);
			_syncService.SyncPages(CurrentUser);

			return Json(new {Synced = true});
		}
    }
}

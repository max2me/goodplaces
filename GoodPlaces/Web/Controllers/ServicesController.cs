﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GoodPlaces.App.Services;
using GoodPlaces.App.Services.FoursquareModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GoodPlaces.Controllers
{
	public class ServicesController : Controller
	{
		public const string ApiPath = " http://api.geonames.org/";
        private readonly RestaurantService _resService;

        public ServicesController(RestaurantService resService)
        {
            _resService = resService;
        }

		public ActionResult GetPlaceInfo(string lat, string lon)
		{
			var client = new RestClient(ApiPath);
			var request = new RestRequest("findNearestAddressJSON");

			request.AddParameter("lat", lat);
			request.AddParameter("lng", lon);
			request.AddParameter("username", "martooooon");

			try
			{
				var response = client.Execute(request);
				var s = JsonConvert.DeserializeObject<JObject>(response.Content)["address"].ToString();
				var address = JsonConvert.DeserializeObject<GeoAddress>(s);
				return Json(address, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(new {}, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult GetVenuesByCity(string lat, string lon, string query)
		{
			var venues = FoursquareService.GetVenuesByCity(double.Parse(lat), double.Parse(lon), query);

			var venueMap = venues.Select(x => new VenueAutoComplete(x.Name, x.Id, x.Location)).ToList();
			return Json(venueMap, JsonRequestBehavior.AllowGet);
		}

        public ActionResult GetCityStateFormatted()
        {
            var cities = _resService.GetCityStateDelimited();

            return Json(cities, JsonRequestBehavior.AllowGet);
        }
	}

	public class GeoAddress
	{
		public string placename { get; set; }
		public string adminCode1 { get; set; }
	}

	public class VenueAutoComplete
	{
		public VenueAutoComplete(string label, string id, GoodPlaces.App.Services.FoursquareModel.Location location)
		{
			this.label = String.Format("{0} in {1}", label, location.City);
			this.value = label;
			this.id = id;
		}

		public string id{ get; set; }
		public string label { get; set; }
		public string value { get; set; }
	}
}
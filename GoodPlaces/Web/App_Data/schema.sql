
    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKBC1A12D40C76DD1]') AND parent_object_id = OBJECT_ID('PageFriend'))
alter table PageFriend  drop constraint FKBC1A12D40C76DD1


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKBC1A12D182E78A3]') AND parent_object_id = OBJECT_ID('PageFriend'))
alter table PageFriend  drop constraint FKBC1A12D182E78A3


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK80B01587D6043AD9]') AND parent_object_id = OBJECT_ID('UserFriend'))
alter table UserFriend  drop constraint FK80B01587D6043AD9


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK80B0158762B13040]') AND parent_object_id = OBJECT_ID('UserFriend'))
alter table UserFriend  drop constraint FK80B0158762B13040


    if exists (select * from dbo.sysobjects where id = object_id(N'[CachedFacebookQuery]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [CachedFacebookQuery]

    if exists (select * from dbo.sysobjects where id = object_id(N'[Friend]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [Friend]

    if exists (select * from dbo.sysobjects where id = object_id(N'[Page]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [Page]

    if exists (select * from dbo.sysobjects where id = object_id(N'PageFriend') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table PageFriend

    if exists (select * from dbo.sysobjects where id = object_id(N'[User]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [User]

    if exists (select * from dbo.sysobjects where id = object_id(N'UserFriend') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table UserFriend

    create table [CachedFacebookQuery] (
        Id UNIQUEIDENTIFIER not null,
       Query nvarchar(4000) null,
       AccessToken NVARCHAR(255) null,
       Response ntext null,
       primary key (Id)
    )

    create table [Friend] (
        Id BIGINT not null,
       FirstName NVARCHAR(255) null,
       LastName NVARCHAR(255) null,
       Photo NVARCHAR(255) null,
       primary key (Id)
    )

    create table [Page] (
        Id BIGINT not null,
       Name NVARCHAR(255) null,
       Phone NVARCHAR(255) null,
       About NVARCHAR(500) null,
       Location_Street NVARCHAR(255) null,
       Location_City NVARCHAR(255) null,
       Location_State NVARCHAR(255) null,
       Location_Zip NVARCHAR(255) null,
       Location_Country NVARCHAR(255) null,
       Location_Latitude DECIMAL(19,5) null,
       Location_Longitude DECIMAL(19,5) null,
       Specialties_Breakfast BIT null,
       Specialties_Lunch BIT null,
       Specialties_Dinner BIT null,
       Specialties_Coffee BIT null,
       Specialties_Drinks BIT null,
       primary key (Id)
    )

    create table PageFriend (
        Page_id BIGINT not null,
       Friend_id BIGINT not null,
       primary key (Page_id, Friend_id)
    )

    create table [User] (
        Id UNIQUEIDENTIFIER not null,
       FirstName NVARCHAR(255) null,
       LastName NVARCHAR(255) null,
       FacebookAccessToken NVARCHAR(255) null,
       FacebookId BIGINT null,
       primary key (Id)
    )

    create table UserFriend (
        Usser UNIQUEIDENTIFIER not null,
       Friend BIGINT not null,
       primary key (Usser, Friend)
    )

    create index IX_CachedFacebookQueryMap_Query on [CachedFacebookQuery] (AccessToken)

    alter table PageFriend 
        add constraint FKBC1A12D40C76DD1 
        foreign key (Friend_id) 
        references [Friend]

    alter table PageFriend 
        add constraint FKBC1A12D182E78A3 
        foreign key (Page_id) 
        references [Page]

    create index IX_User_FacebookId on [User] (FacebookId)

    alter table UserFriend 
        add constraint FK80B01587D6043AD9 
        foreign key (Friend) 
        references [Friend]

    alter table UserFriend 
        add constraint FK80B0158762B13040 
        foreign key (Usser) 
        references [User]

﻿using System;
using GoodPlaces.App.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GoodplacesTest
{
    [TestClass]
    public class TestFacebook
    {
        // this is really an integration test, you need to re-retrieve an access token and the image url to use it.
        [TestMethod]
        public void TestGetProfilePhoto()
        {
            var accessToken = "AAACEdEose0cBAHZAp6v2KZBQ0taurVZBRUGagmSgZA9O5Lb8FkivKifkxOpF5THhRHw6EI0oHXSnXcQRZBLhyM25cQmqQoUSl1g4w4ontZCgZDZD";
            var result = FacebookService.GetProfileImage(accessToken);
            Assert.AreEqual(
                @"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc6/c64.0.384.384/282569_10101655111290608_1823675827_n.jpg",
                result);
        }
    }
}

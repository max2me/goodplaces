﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using GoodPlaces.App.Services;
using GoodPlaces.App.Services.FoursquareModel;

namespace GoodplacesTest
{
    [TestClass]
    public class TestFoursquare
    {

        // Test the foursqaure venue api
        [TestMethod]
        public void TestVenue()
        {
            VenueDetails v = FoursquareService.GetVenueDetails("4a9441bff964a520f02020e3");
            Assert.AreEqual("Michou", v.Name);
        }
    }
}

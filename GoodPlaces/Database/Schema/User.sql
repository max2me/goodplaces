﻿CREATE TABLE [dbo].[User]
(
	[Id] UniqueIdentifier NOT NULL PRIMARY KEY,
	[FirstName] NVARCHAR(50),
	[LastName] NVARCHAR(50),
	[FacebookId] bigint,
	[FacebookAccessToken] NVARCHAR(200)
)

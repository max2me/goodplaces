﻿CREATE TABLE [dbo].[CachedFacebookQuery]
(
	[Id] UniqueIdentifier NOT NULL PRIMARY KEY,
	[Query] NVARCHAR(2000),
	[Response] ntext
)
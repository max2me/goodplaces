﻿CREATE TABLE [dbo].[Friend]
(
	[Id] bigint NOT NULL PRIMARY KEY,
	[FirstName] NVARCHAR(250),
	[LastName] NVARCHAR(250),
	[Photo] NVARCHAR(250)
)
